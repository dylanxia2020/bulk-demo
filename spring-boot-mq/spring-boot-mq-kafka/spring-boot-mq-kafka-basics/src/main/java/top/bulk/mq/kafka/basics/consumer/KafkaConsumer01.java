package top.bulk.mq.kafka.basics.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import top.bulk.mq.kafka.basics.message.KafkaMessage01;

/**
 * 消费者
 *
 * @author 散装java
 * @date 2023-03-14
 */
@Component
@Slf4j
public class KafkaConsumer01 {
    /**
     * topics topic名称
     * groupId 是设置消费组
     */
    @KafkaListener(topics = KafkaMessage01.TOPIC,
            groupId = "consumer-group-01-" + KafkaMessage01.TOPIC)
    public void onMessage(KafkaMessage01 message) {
        log.info("[KafkaConsumer01][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
    }
}
